package bot.dto;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

public class InlineKeyboardDto {

    List<List<InlineKeyboardButton>> keyboardMarkup;
    public InlineKeyboardDto(int rows) {
        this.keyboardMarkup = new ArrayList<>();
        for (int i = 0; i < rows; i++)
            this.keyboardMarkup.add(new ArrayList<>());
    }

    public void add(int row, String text, String callbackData) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText(text);
        button.setCallbackData(callbackData);
        this.keyboardMarkup.get(row).add(button);
    }

    public InlineKeyboardMarkup getMarkup() {
        InlineKeyboardMarkup keyboardMarkup = new InlineKeyboardMarkup();
        keyboardMarkup.setKeyboard(this.keyboardMarkup);
        return keyboardMarkup;
    }
}

