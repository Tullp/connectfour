package bot.dto;

import bot.GameConfig;
import bot.Main;
import bot.models.Game;
import bot.services.GameService;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;

public class GameDto {

    public static SendMessage getInitMessage(Game game) {
        InlineKeyboardDto keyboardDto = new InlineKeyboardDto(1);
        keyboardDto.add(0, "Join", "join");
        keyboardDto.add(0, "Leave", "leave");

        String text = "<b>Connect Four</b>\n\n";
        if (game.getUser1() == null) text += "<i>Сессия пуста</i>";
        else if (game.getUser2() == null)
            text += String.format("%s ожидает соперника.", game.getUser1().getUsername());

        return SendMessage.builder()
                .chatId(game.getChatId())
                .text(text)
                .parseMode(ParseMode.HTML)
                .replyMarkup(keyboardDto.getMarkup())
                .build();
    }

    public static EditMessageText getGameMessage(Game game) {

        String[] smiles = new String[] { "⚪", "🔵", "🔴", "🔸", "⚫" }; // empty, p1, p2, win cell, loser
        int[][] table = GameService.getInstance().getGameTable(game);

        InlineKeyboardDto keyboardDto = new InlineKeyboardDto(GameConfig.gameRows + 1);
        for (int row = 0; row < GameConfig.gameRows; row += 1)
            for (int col = 0; col < GameConfig.gameCols; col += 1)
                keyboardDto.add(row, smiles[table[row][col]],
                        game.getWinner() == null ? String.format("click %d", col) : "pass"
                );
        if (game.getWinner() == null) keyboardDto.add(GameConfig.gameRows, "Stop Game", "stop");
        else keyboardDto.add(GameConfig.gameRows, "Delete Game", "delete");

        String text = String.format(
                "<b>Connect Four</b>\n\n%s (🔵) vs %s (🔴)\n\n",
                game.getUser1().getUsername(), game.getUser2().getUsername()
        );

        if (game.getWinner() == null) {
            if (game.getStep() == 1) text += String.format(
                    "▶ %s (%s)", game.getUser1().getUsername(), smiles[1]
            );
            else if (game.getStep() == 2) text += String.format(
                    "▶ %s (%s)", game.getUser2().getUsername(), smiles[2]
            );
        }
        else text += String.format("🏆 %s победил!", game.getWinner().getUsername());

        return EditMessageText.builder()
                .chatId(game.getChatId())
                .messageId(game.getMessageId())
                .text(text)
                .parseMode(ParseMode.HTML)
                .replyMarkup(keyboardDto.getMarkup())
                .build();

    }
}
