package bot.services;

import bot.GameConfig;
import bot.dao.GameDao;
import bot.models.*;
import bot.services.StrikeFinders.HorizontalStrikeFinder;
import bot.services.StrikeFinders.MainDiagonalStrikeFinder;
import bot.services.StrikeFinders.SideDiagonalStrikeFinder;
import bot.services.StrikeFinders.VerticalStrikeFinder;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GameService {

    private static GameService instance;
    public static synchronized GameService getInstance() {
        if (instance == null) instance = new GameService();
        return instance;
    }
    
    public Game findGame(int id) {
        return GameDao.getInstance().findById(id);
    }

    public Game findGame(String chatId, int messageId) {
        return GameDao.getInstance().findGameByChatAndMessageId(chatId, messageId);
    }

    public Cell findCell(Game game, int row, int col) {
        return GameDao.getInstance().findCellByRowAndColumn(game, row, col);
    }

    public void save(Object obj) {
        GameDao.getInstance().save(obj);
    }

    public void deleteGame(Game game) {
        GameDao.getInstance().delete(game);
    }

    public void updateGame(Game game) {
        GameDao.getInstance().update(game);
    }

    public void addCell(Game game, Cell cell) { game.addCell(cell); }

    public int[][] getGameTable(Game game) {
        int[][] table = new int[GameConfig.gameRows][GameConfig.gameCols];
        for (Cell cell : game.getCells())
            table[cell.getRow()][cell.getCol()] = cell.getState();
        return table;
    }

    public void userStepHandle(Game game, int selectedColumn) {
        int[][] table = GameService.getInstance().getGameTable(game);
        int selectedRow = 0;
        for (int row = GameConfig.gameRows - 1; row >= 0; row -= 1)
            if (table[row][selectedColumn] == 0) {
                selectedRow = row;
                break;
            }

        Cell selectedCell = new Cell(selectedRow, selectedColumn, game.getStep());
        GameService.getInstance().addCell(game, selectedCell);
        game.setStep(game.getStep() == 1 ? 2 : 1);
        GameService.getInstance().save(selectedCell);

        List<Cell> winCells = GameService.getInstance().getWinCells(game);
        if (winCells != null) {
            int winState = winCells.get(0).getState();
            game.setWinner(winState == 1 ? game.getUser1() : game.getUser2());
            for (Cell cell : game.getCells())
                if (cell.getState() != winState) {
                    cell.setState(4);
                    GameService.getInstance().save(cell);
                }
            for (Cell cell : winCells) {
                cell.setState(3);
                GameService.getInstance().save(cell);
            }
        }

        GameService.getInstance().updateGame(game);
    }

    public List<Cell> getWinCells(Game game) {
        List<Cell> verticalCells = new VerticalStrikeFinder(game).getWinCells();
        List<Cell> horizontalCells = new HorizontalStrikeFinder(game).getWinCells();
        List<Cell> mainDiagonalCells = new MainDiagonalStrikeFinder(game).getWinCells();
        List<Cell> sideDiagonalCells = new SideDiagonalStrikeFinder(game).getWinCells();

        if (verticalCells != null) return verticalCells;
        else if (horizontalCells != null) return horizontalCells;
        else if (mainDiagonalCells != null) return mainDiagonalCells;
        else if (sideDiagonalCells != null) return sideDiagonalCells;

        return null;
    }
}
