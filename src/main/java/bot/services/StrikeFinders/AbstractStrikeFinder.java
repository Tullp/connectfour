package bot.services.StrikeFinders;

import bot.GameConfig;
import bot.models.Cell;
import bot.models.Game;
import bot.services.GameService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class AbstractStrikeFinder {

	private Game game;
	public AbstractStrikeFinder(Game game) { this.game = game; }

	public abstract int getRowStart();
	public abstract int getRowFinish();
	public abstract int getColStart();
	public abstract int getColFinish();
	public abstract int getRow(int row, int i);
	public abstract int getCol(int col, int i);

	public List<Cell> getWinCells() {
		int[][] table = GameService.getInstance().getGameTable(game);
		for (int row = getRowStart(); row < getRowFinish(); row++) // горизонталь
			for (int col = getColStart(); col < getColFinish(); col++)
				if (table[row][col] != 0) {
					int finalRow = row;
					int finalCol = col;
					if (IntStream.range(0, GameConfig.winCellsCount).map(i -> table[getRow(finalRow, i)][getCol(finalCol, i)]).distinct().count() == 1) {
						return IntStream.range(0, GameConfig.winCellsCount)
							.mapToObj(i -> GameService.getInstance().findCell(game, getRow(finalRow, i), getCol(finalCol, i)))
							.collect(Collectors.toList());
					}
				}
		return null;
	}

}
