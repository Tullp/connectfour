package bot.services.StrikeFinders;

import bot.GameConfig;
import bot.models.Game;

public class SideDiagonalStrikeFinder extends AbstractStrikeFinder{

	public SideDiagonalStrikeFinder(Game game) {
		super(game);
	}

	@Override
	public int getRowStart() {
		return 0;
	}

	@Override
	public int getRowFinish() {
		return GameConfig.gameRows - GameConfig.winCellsCount + 1;
	}

	@Override
	public int getColStart() {
		return GameConfig.winCellsCount - 1;
	}

	@Override
	public int getColFinish() {
		return GameConfig.gameCols;
	}

	@Override
	public int getRow(int row, int i) {
		return row + i;
	}

	@Override
	public int getCol(int col, int i) {
		return col - i;
	}
}
