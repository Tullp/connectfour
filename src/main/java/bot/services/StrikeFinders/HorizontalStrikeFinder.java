package bot.services.StrikeFinders;

import bot.GameConfig;
import bot.models.Game;

public class HorizontalStrikeFinder extends AbstractStrikeFinder {

	public HorizontalStrikeFinder(Game game) {
		super(game);
	}

	@Override
	public int getRowStart() {
		return 0;
	}

	@Override
	public int getRowFinish() {
		return GameConfig.gameRows;
	}

	@Override
	public int getColStart() {
		return 0;
	}

	@Override
	public int getColFinish() {
		return GameConfig.gameCols - GameConfig.winCellsCount + 1;
	}

	@Override
	public int getRow(int row, int i) {
		return row;
	}

	@Override
	public int getCol(int col, int i) {
		return col + i;
	}
}
