package bot.dao;

import bot.models.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import bot.utils.HibernateSessionFactoryUtil;
import org.hibernate.query.Query;

public class GameDao {

    private static GameDao instance;
    public static synchronized GameDao getInstance() {
        if (instance == null) instance = new GameDao();
        return instance;
    }

    public Game findById(int id) {
        return HibernateSessionFactoryUtil.getSession().get(Game.class, id);
    }

    public Game findGameByChatAndMessageId(String chatId, int messageId) {
        Session session = HibernateSessionFactoryUtil.getSession();
        Query<Game> query = session.createQuery("from Game g where g.chatId =: chatId and g.messageId =: messageId", Game.class);
        query.setParameter("chatId", chatId);
        query.setParameter("messageId", messageId);
        return query.uniqueResult();
    }

    public Cell findCellByRowAndColumn(Game game, int row, int col) {
        Session session = HibernateSessionFactoryUtil.getSession();
        Query<Cell> query = session.createQuery("from Cell c where c.game =: game and c.row =: row and c.col =: col", Cell.class);
        query.setParameter("game", game);
        query.setParameter("row", row);
        query.setParameter("col", col);
        return query.uniqueResult();
    }

    public void save(Object obj) {
        Session session = HibernateSessionFactoryUtil.getSession();
        Transaction transaction = session.beginTransaction();
        session.save(obj);
        transaction.commit();
    }

    public void update(Game game) {
        Session session = HibernateSessionFactoryUtil.getSession();
        Transaction transaction = session.beginTransaction();
        session.update(game);
        transaction.commit();
    }

    public void delete(Game game) {
        Session session = HibernateSessionFactoryUtil.getSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(game);
        tx1.commit();
    }
}
