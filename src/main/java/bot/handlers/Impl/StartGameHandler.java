package bot.handlers.Impl;

import bot.dto.GameDto;
import bot.handlers.Command;
import bot.handlers.ITextHandler;
import bot.models.Game;
import bot.models.User;
import bot.services.GameService;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Arrays;
import java.util.List;

@Command
public class StartGameHandler implements ITextHandler {

    TelegramLongPollingBot bot;
    public StartGameHandler(TelegramLongPollingBot bot) {
        this.bot = bot;
    }

    public List<String> getCommands() {
        return Arrays.asList("/connectfour", "/connectfour@" + this.bot.getBotUsername());
    }

    public boolean isSatisfied(Message message) {
        return message.getChatId() != (long)message.getFrom().getId();
    }

    @Override
    public void handle(Update update) throws TelegramApiException {
        Message message = update.getMessage();

        Game game = new Game(message.getChatId().toString(), new User(
                message.getFrom().getId(),
                message.getFrom().getFirstName(),
                message.getFrom().getUserName()
        ));
        GameService.getInstance().save(game.getUser1());
        GameService.getInstance().save(game);

        Message gameMessage = this.bot.execute(GameDto.getInitMessage(game));
        game.setMessageId(gameMessage.getMessageId());
        GameService.getInstance().updateGame(game);
    }
}
