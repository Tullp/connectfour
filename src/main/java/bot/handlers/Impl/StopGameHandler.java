package bot.handlers.Impl;

import bot.handlers.ICallbackQueryHandler;
import bot.handlers.Command;
import bot.models.Game;
import bot.services.GameService;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Arrays;
import java.util.List;

@Command()
public class StopGameHandler implements ICallbackQueryHandler {

    TelegramLongPollingBot bot;
    public StopGameHandler(TelegramLongPollingBot bot) { this.bot = bot; }

    public List<String> getCommands() { return Arrays.asList("stop", "delete"); }

    @Override
    public void handle(Update update) throws TelegramApiException {
        CallbackQuery query = update.getCallbackQuery();
        Message message = query.getMessage();
        Game game = GameService.getInstance().findGame(message.getChatId().toString(), message.getMessageId());

        if (query.getFrom().getId() != game.getUser1().getUserId() && query.getFrom().getId() != game.getUser2().getUserId()) return;

        GameService.getInstance().deleteGame(game);

        this.bot.execute(DeleteMessage.builder()
                .chatId(message.getChatId().toString())
                .messageId(message.getMessageId())
                .build()
        );
    }
}
