package bot.handlers.Impl;

import bot.dto.GameDto;
import bot.handlers.ICallbackQueryHandler;
import bot.handlers.Command;
import bot.models.Game;
import bot.services.GameService;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Arrays;
import java.util.List;

@Command()
public class LeaveGameHandler implements ICallbackQueryHandler {

    TelegramLongPollingBot bot;
    public LeaveGameHandler(TelegramLongPollingBot bot) { this.bot = bot; }

    public List<String> getCommands() { return Arrays.asList("leave"); }

    @Override
    public void handle(Update update) throws TelegramApiException {
        CallbackQuery query = update.getCallbackQuery();
        Message message = query.getMessage();
        Game game = GameService.getInstance().findGame(message.getChatId().toString(), message.getMessageId());

        if (game.getUser2().getUserId() == query.getFrom().getId()) game.setUser2(null);
        else if (game.getUser1().getUserId() == query.getFrom().getId()) {
            game.setUser1(game.getUser2());
            game.setUser2(null);
        }
        GameService.getInstance().updateGame(game);

        this.bot.execute(GameDto.getInitMessage(game));
    }
}
