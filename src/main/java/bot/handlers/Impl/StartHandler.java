package bot.handlers.Impl;

import bot.handlers.Command;
import bot.handlers.ITextHandler;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Arrays;
import java.util.List;

@Command()
public class StartHandler implements ITextHandler {

    TelegramLongPollingBot bot;
    public StartHandler(TelegramLongPollingBot bot) {
        this.bot = bot;
    }

    @Override
    public List<String> getCommands() { return Arrays.asList("/start"); }

    @Override
    public boolean isSatisfied(Message message) {
        return message.getChatId() == (long)message.getFrom().getId();
    }

    @Override
    public void handle(Update update) throws TelegramApiException {
        Message message = update.getMessage();
        System.out.println(message.getText());
        this.bot.execute(SendMessage.builder()
                .chatId(message.getChatId().toString())
                .text("Привет! Чтобы начать игру, надо в чате написать команду /connectfour")
                .build()
        );
    }
}
