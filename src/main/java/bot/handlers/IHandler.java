package bot.handlers;

import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public interface IHandler {
    boolean isRelate(Update update);
    void handle(Update update) throws TelegramApiException;
}
