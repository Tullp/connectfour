package bot.handlers;

import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

public interface ITextHandler extends IHandler {

    default List<String> getCommands() {
        return null;
    }

    default String getRegex() {
        return null;
    }

    default boolean isSatisfied(Message message) {
        return true;
    }

    default boolean isRelate(Update update) {
        if (!update.hasMessage() || !update.getMessage().hasText()) return false;

        Message message = update.getMessage();

        if (this.getCommands() != null) {
            String command = message.getText().split(" ")[0];
            if (!this.getCommands().contains(command)) return false;
        }

        if (this.getRegex() != null && !message.getText().matches(this.getRegex())) return false;

        //noinspection RedundantIfStatement
        if (!this.isSatisfied(message)) return false;

        return true;
    }
}
