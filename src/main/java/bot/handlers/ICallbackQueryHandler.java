package bot.handlers;

import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

public interface ICallbackQueryHandler extends IHandler {

    default List<String> getCommands() {
        return null;
    }

    default String getRegex() {
        return null;
    }

    default boolean isSatisfied(CallbackQuery query) { return true; }

    default boolean isRelate(Update update) {
        if (!update.hasCallbackQuery()) return false;

        CallbackQuery query = update.getCallbackQuery();

        if (this.getCommands() != null) {
            String command = query.getData().split(" ")[0];
            if (!this.getCommands().contains(command)) return false;
        }

        if (this.getRegex() != null && !query.getData().matches(this.getRegex())) return false;

        //noinspection RedundantIfStatement
        if (!this.isSatisfied(query)) return false;

        return true;
    }
}
