package bot;

import bot.core.ConnectFourBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) throws TelegramApiException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        String username = args[0];
        String token = args[1];
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
        telegramBotsApi.registerBot(new ConnectFourBot(username, token));
    }
}
