package bot.core;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.lang.reflect.InvocationTargetException;

public class ConnectFourBot extends TelegramLongPollingBot {

    UpdateReceiver updateReceiver;
    private final String username;
    private final String token;

    public ConnectFourBot(String username, String token) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        this.updateReceiver = new UpdateReceiver(this);
        this.username = username;
        this.token = token;
    }

    @Override
    public String getBotUsername() {
        return this.username;
    }

    @Override
    public String getBotToken() {
        return this.token;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            this.updateReceiver.handle(update);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
