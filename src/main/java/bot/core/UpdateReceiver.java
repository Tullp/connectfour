package bot.core;

import bot.Main;
import bot.handlers.Command;
import bot.handlers.IHandler;
import org.reflections.Reflections;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UpdateReceiver {

    TelegramLongPollingBot bot;
    private final List<IHandler> handlers = new ArrayList<IHandler>();

    public UpdateReceiver(TelegramLongPollingBot bot) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        this.bot = bot;
        Reflections reflections = new Reflections(Main.class.getPackage().getName());
        Set<Class<?>> classes = reflections.getTypesAnnotatedWith(Command.class);
        for (Class cls : classes)
            this.AddHandler((IHandler)cls.getConstructor(TelegramLongPollingBot.class).newInstance(this.bot));
    }

    public void AddHandler(IHandler handler) {
        this.handlers.add(handler);
    }

    public void handle(Update update) throws TelegramApiException {
        for (IHandler handler : this.findHandlers(update))
            handler.handle(update);
    }

    private List<IHandler> findHandlers(Update update) {
        return handlers.stream().filter(handler -> handler.isRelate(update)).collect(Collectors.toList());
    }
}
