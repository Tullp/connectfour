package bot.models;

import javax.persistence.*;
@Entity
@Table (name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "user_id")
    private int userId;
    private String name;
    private String username;

    public User() { }

    public User(int userId, String name, String username) {
        this.userId = userId;
        this.name = name;
        this.username = username;
    }

    public int getId() {
        return this.id;
    }

    public int getUserId() {
        return this.userId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return String.format(
                "bot.models.User(id: %d, userId: %d, username: '%s', name: '%s')",
                this.id, this.userId, this.username, this.name
        );
    }
}