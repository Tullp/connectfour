package bot.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Entity
@Table(name = "games")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int step;

    @Column(name = "chat_id")
    private String chatId;

    @Column(name = "message_id")
    private int messageId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id1")
    private User user1;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id2")
    private User user2;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "winner_id")
    private User winner;

    @OneToMany(mappedBy = "game", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Cell> cells;

    public Game() { }

    public Game(String chatId, User user) {
        this.chatId = chatId;
        this.user1 = user;
        this.cells = new ArrayList<>();
        this.step = Math.abs(new Random().nextInt()) % 2 + 1;
    }

    public int getId() {
        return this.id;
    }


    public int getStep() {
        return this.step;
    }

    public void setStep(int step) {
        this.step = step;
    }


    public String getChatId() {
        return this.chatId;
    }


    public int getMessageId() {
        return this.messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }


    public User getUser1() {
        return this.user1;
    }

    public void setUser1(User user) {
        this.user1 = user;
    }


    public User getUser2() {
        return this.user2;
    }

    public void setUser2(User user) {
        this.user2 = user;
    }


    public User getWinner() {
        return this.winner;
    }

    public void setWinner(User winner) {
        this.winner = winner;
    }

    public List<Cell> getCells() { return this.cells; }

    public void addCell(Cell cell) {
        this.cells.add(cell);
        cell.setGame(this);
    }


    @Override
    public String toString() {
        return String.format(
                "bot.models.Game(id: %d, chatId: %s, messageId: %d, step: %d, user1: %s, user2: %s, cells: %s)",
                this.id, this.chatId, this.messageId, this.step, this.user1.toString(), this.user2.toString(),
                this.cells.stream().map(Cell::toString).collect(Collectors.joining(", "))
        );
    }
}
