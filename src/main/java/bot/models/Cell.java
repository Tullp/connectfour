package bot.models;

import javax.persistence.*;

@Entity
@Table(name = "cells")
public class Cell {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int row;
    private int col;
    private int state;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private Game game;

    public Cell() { }

    public Cell(int row, int col, int state) {
        this.row = row;
        this.col = col;
        this.state = state;
    }

    public int getId() { return this.id; }

    public int getRow() { return this.row; }

    public int getCol() { return this.col; }

    public int getState() { return this.state; }

    public void setState(int state) { this.state = state; }

    public Game getGame() { return this.game; }

    public void setGame(Game game) { this.game = game; }

    @Override
    public String toString() {
        return String.format(
                "bot.models.Cell(id: %d, row: %d, col: %d, gameId: %d)",
                this.id, this.row, this.col, this.game.getId()
        );
    }
}
